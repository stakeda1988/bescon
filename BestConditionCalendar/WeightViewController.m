//
//  WeightViewController.m
//  BestConditionCalendar
//
//  Created by SHOKI TAKEDA on 8/11/15.
//  Copyright (c) 2015 misakishiki.com. All rights reserved.
//

#import "WeightViewController.h"
#import "AppDelegate.h"
#import "Weight.h"

@interface WeightViewController ()
@property (strong, nonatomic) NSManagedObjectContext *conext;
@end

@implementation WeightViewController
@synthesize weightInfoViewController = _weightInfoViewController;
@synthesize fetchedResultsController = __fetchedResultsController;
@synthesize managedObjectContext = __managedObjectContext;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // データ取得用のオブジェクトであるFetchオブジェクトを作成
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([Weight class])];
    
    // Sort条件を設定（日付順で降順）
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:NO];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // manageObjectContextからデータを取得
    NSArray *results = [self.conext executeFetchRequest:fetchRequest error:nil];
    for (Weight *weight in results) {
        NSLog(@"------------------------------------");
        NSLog(@"%@", [weight weight]);
        NSLog(@"%@", [weight date]);
        NSLog(@"------------------------------------");
    }
    
    
    self.cells = [NSMutableArray array];
//    self.cells = (NSMutableArray *)results;
    self.tv.dataSource = self;
    self.conext = [(AppDelegate *)[[UIApplication sharedApplication]delegate] managedObjectContext];
    self.weightInfoViewController.managedObjectContext = self.managedObjectContext;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backToTop:(id)sender {
    [self dismissModalViewControllerAnimated:NO];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.cells count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *tvcell = [tableView dequeueReusableCellWithIdentifier: @"cid"];
    if (tvcell == nil) {
        tvcell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                        reuseIdentifier: @"cid"];
    }
    tvcell.textLabel.text = [self.cells objectAtIndex: indexPath.row];
    tvcell.textAlignment = UITextAlignmentCenter;
    tvcell.textColor = [UIColor colorWithRed:0.2 green:0.8 blue:0.8 alpha:1.0];
    tvcell.font = [UIFont fontWithName:@"Helvetica" size:22];
    tvcell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return tvcell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.cells removeObjectAtIndex:indexPath.row];
    [self.tv reloadData];
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // The table view should not be re-orderable.
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // データ取得用のオブジェクトであるFetchオブジェクトを作成
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([Weight class])];
    
    // Sort条件を設定（日付順で降順）
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:NO];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // manageObjectContextからデータを取得
    NSArray *results = [self.conext executeFetchRequest:fetchRequest error:nil];
    for (Weight *weight in results) {
        NSLog(@"------------------------------------");
        NSLog(@"%@", [weight weight]);
        NSLog(@"%@", [weight date]);
        NSLog(@"------------------------------------");
    }
}

- (IBAction)insertCell:(id)sender {
    NSDate *tmpDate = [NSDate date];
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"yyyy/MM/dd HH:mm"];
    NSString *strNow = [outputFormatter stringFromDate:tmpDate];
    // CoreDataへのインサート用オブジェクトを作成
    Weight *weight = [NSEntityDescription insertNewObjectForEntityForName:@"Weight" inManagedObjectContext:self.conext];
    
    // CoreDataのインサート用オブジェクトにデータをセット
    [weight setDate:tmpDate];
    [weight setWeight:@""];
    
    // CoreDataへデータを保存
    NSError *error;
    if (![self.conext save:&error]) {
        NSLog(@"error = %@", error);
    } else {
        NSLog(@"成功");
    }
    
    [self.cells addObject: [[NSString alloc] initWithFormat:
                            @"%@", strNow]];
    [self.tv reloadData];
    WeightInfoViewController *weightInfoVC = [[WeightInfoViewController alloc] init];
    [self presentViewController: weightInfoVC animated:YES completion: nil];
}
@end
