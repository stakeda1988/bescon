//
//  PlotViewController.h
//  BestConditionCalendar
//
//  Created by SHOKI TAKEDA on 8/22/15.
//  Copyright (c) 2015 misakishiki.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"

@interface PlotViewController : UIViewController<CPTPlotDataSource>
{
@private
    // グラフ表示領域（この領域に棒グラフを追加する）
    CPTGraph *graph;
}
- (IBAction)moveToTopPage:(id)sender;

// 表示するデータを保持する配列
@property(nonatomic, strong) NSMutableArray *dataForBar;        // 棒グラフ用
@property(nonatomic, strong) NSMutableArray *dataForScatter;    // 折れ線グラフ用
@end
