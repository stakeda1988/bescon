//
//  ViewController.h
//  BestConditionCalendar
//
//  Created by SHOKI TAKEDA on 8/11/15.
//  Copyright (c) 2015 misakishiki.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
- (IBAction)moveToPlot:(id)sender;
- (IBAction)moveToCalendar:(id)sender;
@end

