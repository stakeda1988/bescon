//
//  Weight.h
//  BestConditionCalendar
//
//  Created by SHOKI TAKEDA on 8/11/15.
//  Copyright (c) 2015 misakishiki.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Weight : NSManagedObject

@property (nonatomic, retain) NSString * weight;
@property (nonatomic, retain) NSDate * date;

@end
