//
//  WeightInfoViewController.m
//  BestConditionCalendar
//
//  Created by SHOKI TAKEDA on 8/11/15.
//  Copyright (c) 2015 misakishiki.com. All rights reserved.
//

#import "WeightInfoViewController.h"

@interface WeightInfoViewController ()
@property (strong, nonatomic) NSManagedObjectContext *conext;
@end

@implementation WeightInfoViewController
@synthesize conext;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)loadView
{
    [super loadView];
    UIColor *customColor = [UIColor colorWithRed:0.2 green:0.8 blue:0.8 alpha:1.0];
    self.view.backgroundColor = customColor;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.conext = [(AppDelegate *)[[UIApplication sharedApplication]delegate] managedObjectContext];
    int screenWidth = self.view.frame.size.width;
    
    UIButton *backButton = [[UIButton alloc] init];
    
    backButton.frame = CGRectMake(-20, 40, 150, 20);
    [backButton setTitle:@"キャンセル" forState:UIControlStateNormal];
    [backButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [backButton addTarget:self
                   action:@selector(backToTop:)
         forControlEvents:UIControlEventTouchUpInside];
    
    // ボタンをビューに追加
    [self.view addSubview:backButton];
    
    UIButton *doneButton = [[UIButton alloc] init];
    
    doneButton.frame = CGRectMake(200, 40, 150, 20);
    [doneButton setTitle:@"完了" forState:UIControlStateNormal];
    [doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [doneButton addTarget:self
                   action:@selector(doneNewObject:)
         forControlEvents:UIControlEventTouchUpInside];
    
    // ボタンをビューに追加
    [self.view addSubview:doneButton];
    
    UILabel *navLabel = [[UILabel alloc] init];
    
    navLabel.frame = CGRectMake((screenWidth/2 - 150/2), 40, 150, 20);
    navLabel.textAlignment = NSTextAlignmentCenter;
    navLabel.text = @"体重情報";
    
    navLabel.textColor = [UIColor whiteColor];
    [self.view addSubview:navLabel];
    
    UIView* pickerBaseView = [[UIView alloc]initWithFrame:CGRectMake(-75, -150, 200, 500)];
    pickerBaseView.backgroundColor = [UIColor clearColor] ;
    pickerBaseView.userInteractionEnabled = true;
    
    UIPickerView* aPicker = [[UIPickerView alloc] init];
    CGRect pickerFrame = aPicker.frame ;
    pickerFrame.size.width = 100 ;
    pickerFrame.size.height = 162.0 ;
    aPicker.frame = pickerFrame ;
    aPicker.center = self.view.center; // 中央に表示
    aPicker.delegate = self;
    aPicker.dataSource = self;
    aPicker.showsSelectionIndicator = YES;
    aPicker.backgroundColor = [UIColor colorWithRed:0.2 green:0.8 blue:0.8 alpha:1.0];
    aPicker.tag = 1;
    
    [ pickerBaseView addSubview:aPicker ] ;
    [ self.view addSubview:pickerBaseView ] ;
    //一番後ろに表示
    [ self.view sendSubviewToBack:pickerBaseView ];
    
    UILabel *dotLabel = [[UILabel alloc] init];
    
    dotLabel.frame = CGRectMake(140, 130, 20, 20);
    dotLabel.textAlignment = NSTextAlignmentCenter;
    dotLabel.text = @".";
    
    dotLabel.textColor = [UIColor blackColor];
    [self.view addSubview:dotLabel];
    
//    UIView* pickerDotBaseView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 200, 500)];
//    pickerDotBaseView.backgroundColor = [UIColor clearColor] ;
//    pickerDotBaseView.userInteractionEnabled = true;
//    
//    UIPickerView* dPicker = [[UIPickerView alloc] init];
//    CGRect dotPickerFrame = dPicker.frame ;
//    dotPickerFrame.size.width = 120 ;
//    dotPickerFrame.size.height = 180 ;
//    dPicker.frame = pickerFrame ;
//    dPicker.center = self.view.center; // 中央に表示
//    dPicker.delegate = self;
//    dPicker.dataSource = self;
//    dPicker.showsSelectionIndicator = YES;
//    dPicker.backgroundColor = [UIColor colorWithRed:0.2 green:0.8 blue:0.8 alpha:1.0];
//    dPicker.tag = 2;
//    
//    [ pickerBaseView addSubview:dPicker ] ;
//    [ self.view addSubview:pickerDotBaseView ] ;
//    //一番後ろに表示
//    [ self.view sendSubviewToBack:pickerDotBaseView ];
    
    UILabel *kgLabel = [[UILabel alloc] init];
    kgLabel.frame = CGRectMake(180, 130, 20, 20);
    kgLabel.textAlignment = NSTextAlignmentCenter;
    kgLabel.text = @"kg";
    kgLabel.textColor = [UIColor blackColor];
    [self.view addSubview:kgLabel];
    
    UITextField *dateField = [[UITextField alloc] init];
    dateField.frame = CGRectMake(10, 320, 300, 40);
    dateField.borderStyle = UITextBorderStyleRoundedRect;
    dateField.textAlignment = NSTextAlignmentCenter;
    dateField.backgroundColor = [UIColor clearColor];
    NSDate *tmpDate = [NSDate date];
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"yyyy/MM/dd HH:mm"];
    // 日付から文字列に変換
    NSString *strNow = [outputFormatter stringFromDate:tmpDate];
    dateField.text = strNow;
    dateField.textColor = [UIColor blackColor];
    [self.view addSubview:dateField];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backToTop:(id)sender {
    [self dismissModalViewControllerAnimated:NO];
}

- (IBAction)doneNewObject:(id)sender {
    
    [self dismissModalViewControllerAnimated:NO];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView*)pickerView{
    return 3;
}

// 行数を返す例
-(NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component{
    if(component == 0){
        return 2;
    }else if(component == 1){
        return 10;
    }else {
        return 10;
    }
}

// 表示する内容を返す例
-(NSString*)pickerView:(UIPickerView*)pickerView
           titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    // 行インデックス番号を返す
//    if (pickerView.tag == 1) {
    return [NSString stringWithFormat:@"%ld", (long)row];
}

- (void)pickerView:(UIPickerView *)pickerView
      didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
//    if (pickerView.tag == 1) {
        // 1列目の選択された行数を取得
    NSInteger val0 = [pickerView selectedRowInComponent:0];
    
    // 2列目の選択された行数を取得
    NSInteger val1 = [pickerView selectedRowInComponent:1];
    
    // 3列目の選択された行数を取得
    NSInteger val2 = [pickerView selectedRowInComponent:2];
    
    NSLog(@"1列目:%ld行目が選択", (long)val0);
    NSLog(@"2列目:%ld行目が選択", (long)val1);
    NSLog(@"3列目:%ld行目が選択", (long)val2);
}

@end
