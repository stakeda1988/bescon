//
//  WeightViewController.h
//  BestConditionCalendar
//
//  Created by SHOKI TAKEDA on 8/11/15.
//  Copyright (c) 2015 misakishiki.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WeightInfoViewController.h"
#import "AppDelegate.h"
#import "Weight.h"

@interface WeightViewController : UIViewController<NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate> {
    IBOutlet UILabel *textLabel;
    NSArray *itemArray;
    NSString  *message;
}

@property (strong, nonatomic) WeightInfoViewController *weightInfoViewController;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain) NSString *message;
- (IBAction)backToTop:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tv;
@property NSMutableArray *cells;
- (IBAction)insertCell:(id)sender;

@end
