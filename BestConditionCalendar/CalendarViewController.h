//
//  CalendarViewController.h
//  BestConditionCalendar
//
//  Created by SHOKI TAKEDA on 8/11/15.
//  Copyright (c) 2015 misakishiki.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HealthViewController.h"

@interface CalendarViewController : UIViewController
@property (nonatomic) NSString *originText;
- (IBAction)backToTopPage:(id)sender;
-(void)buttonTapped:(UIButton *)button;
@end
