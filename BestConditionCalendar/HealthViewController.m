//
//  HealthViewController.m
//  BestConditionCalendar
//
//  Created by SHOKI TAKEDA on 8/11/15.
//  Copyright (c) 2015 misakishiki.com. All rights reserved.
//

#import "HealthViewController.h"

@interface HealthViewController ()

@end

@implementation HealthViewController
@synthesize message;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)loadView
{
    [super loadView];
    UIColor *customColor = [UIColor colorWithRed:0.2 green:0.8 blue:0.8 alpha:1.0];
    self.view.backgroundColor = customColor;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UILabel *secondLabel = [[UILabel alloc] init];
    int screenWidth = self.view.frame.size.width;
    
    secondLabel.frame = CGRectMake((screenWidth/2 - 150/2), 40, 150, 20);
    secondLabel.textAlignment = NSTextAlignmentCenter;
    secondLabel.text = message;
    
    secondLabel.textColor = [UIColor whiteColor];
    [self.view addSubview:secondLabel];
    
    UIButton *backButton = [[UIButton alloc] init];
    
    backButton.frame = CGRectMake(10, 40, 150, 20);
    [backButton setTitle:@"◀︎" forState:UIControlStateNormal];
    [backButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [backButton addTarget:self
               action:@selector(backToTop:)
     forControlEvents:UIControlEventTouchUpInside];
    
    // ボタンをビューに追加
    [self.view addSubview:backButton];
    
    UITableView *table = [[UITableView alloc] init];
    table.frame = CGRectMake(0, 100, 320, 480);
    table.delegate = self;
    table.dataSource = self;
    [self.view addSubview:table];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backToTop:(id)sender {
    [self dismissModalViewControllerAnimated:NO];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    itemArray = [[NSArray alloc] initWithObjects:@"体重", @"体脂肪率", @"睡眠時間", @"体温", @"食べたもの", @"おしっこ", @"うんち", @"サプリメント", @"薬", @"性行為", @"頭の回転", @"運動", @"思ったこと", @"体調", @"受けた施術", nil];
    return [itemArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    cell = [[UITableViewCell alloc] init];
    cell.textLabel.text = [itemArray objectAtIndex: indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([indexPath row] == 0) {
        WeightViewController *weightVC = [[WeightViewController alloc] init];
        [self presentViewController: weightVC animated:YES completion: nil];
    }
}
@end
