//
//  ViewController.m
//  BestConditionCalendar
//
//  Created by SHOKI TAKEDA on 8/11/15.
//  Copyright (c) 2015 misakishiki.com. All rights reserved.
//

#import "ViewController.h"
#import "CalendarViewController.h"
#import "PlotViewController.h"

@interface ViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray *dataSourceiPhone;
@property (nonatomic, strong) NSArray *dataSourceAndroid;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    // デリゲートメソッドをこのクラスで実装する
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    // テーブルに表示したいデータソースをセット
    self.dataSourceiPhone = @[@"最新サプリ情報をここに掲示する", @"最新サプリ情報をここに掲示する", @"最新サプリ情報をここに掲示する", @"最新サプリ情報をここに掲示する", @"最新サプリ情報をここに掲示する"];
    self.dataSourceAndroid = @[@"健康ブログ情報をここに掲示する", @"健康ブログ情報をここに掲示する", @"健康ブログ情報をここに掲示する"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)moveToPlot:(id)sender {
    PlotViewController *plotVC = [[PlotViewController alloc] init];
    [self presentViewController: plotVC animated:YES completion: nil];
}

- (IBAction)moveToCalendar:(id)sender {
    CalendarViewController *firstVC = [[CalendarViewController alloc] init];
    [self presentViewController: firstVC animated:YES completion: nil];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return @"サプリメント情報";
        case 1:
            return @"健康情報";
        default:
            break;
    }
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger dataCount;
    
    // テーブルに表示するデータ件数を返す
    switch (section) {
        case 0:
            dataCount = self.dataSourceiPhone.count;
            break;
        case 1:
            dataCount = self.dataSourceAndroid.count;
            break;
        default:
            break;
    }
    return dataCount;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

/**
 テーブルに表示するセルを返します。（必須）
 
 @return UITableViewCell : テーブルセル
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    // 再利用できるセルがあれば再利用する
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        // 再利用できない場合は新規で作成
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:CellIdentifier];
    }
    
    switch (indexPath.section) {
        case 0:
            cell.textLabel.text = self.dataSourceiPhone[indexPath.row];
            break;
        case 1:
            cell.textLabel.text = self.dataSourceAndroid[indexPath.row];
            break;
        default:
            break;
    }
    
    return cell;
}
@end
