//
//  HealthViewController.h
//  BestConditionCalendar
//
//  Created by SHOKI TAKEDA on 8/11/15.
//  Copyright (c) 2015 misakishiki.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WeightViewController.h"

@interface HealthViewController : UIViewController<UITableViewDataSource, UITableViewDelegate> {
    IBOutlet UILabel *textLabel;
    NSArray *itemArray;
    NSString  *message;
}

@property (nonatomic, retain) NSString *message;
- (IBAction)backToTop:(id)sender;

@end
